# git create

This is just a simple git plugin that clones a repo, removes the 
history and initializes a new archive.

Useful for project templates.


# Install

Place git-create in a directory within your path.
